import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger';
import createPersistedState from "vuex-persistedstate";
import { Toast } from 'vant';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    merchant: [],
  },
  getters: {
  },
  mutations: {
    push_merchat(state, value) {
      state.merchant.push(value)
      Toast({ message: '加入收藏！', icon: 'https://ts1.cn.mm.bing.net/th/id/R-C.75990ca9a3853bd3532e44b689d24675?rik=bVsNpyr9rFvhcg&riu=http%3a%2f%2fwx3.sinaimg.cn%2flarge%2f006APoFYly1go6s4emixlg301t01tweh.gif&ehk=rlYj%2b50wVQKXvUwlA6LF4qhxyM0dGd%2b2X9YE1T6gAFg%3d&risl=&pid=ImgRaw&r=0', });

    },

    clear_merchat(state, value) {
      let delect = state.merchant.findIndex((item) => {
        return item.mtWmPoiId == value.mtWmPoiId;

      })
      if (delect != -1) {
        state.merchant.splice(delect, 1);
      }
      Toast({ message: '移除收藏！！', icon: 'https://ts1.cn.mm.bing.net/th/id/R-C.75990ca9a3853bd3532e44b689d24675?rik=bVsNpyr9rFvhcg&riu=http%3a%2f%2fwx3.sinaimg.cn%2flarge%2f006APoFYly1go6s4emixlg301t01tweh.gif&ehk=rlYj%2b50wVQKXvUwlA6LF4qhxyM0dGd%2b2X9YE1T6gAFg%3d&risl=&pid=ImgRaw&r=0', });

    }
  },
  actions: {
  },
  modules: {
  },
  // state 数据变动控制台提示     将 state 的数据存储在本地存储 
  plugins: [createLogger(), createPersistedState()]
})
