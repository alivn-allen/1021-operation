import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/mylogin',
    name: 'mylogin',
    meta: { title: '首页' },
    component: () => import('../components/MyLogin.vue')
  },
  {
    path: '/myxiangqing/:name',
    name: 'myxiangqing',
    meta: { title: '详情页' },
    component: () => import('../components/MyXiangqing.vue')
  },
  {
    path: '/mycollect',
    name: 'mycollect',
    meta: { title: '收藏页' },
    component: () => import('../components/MyCollect.vue')
  },
  {
    path: '/myindex',
    name: 'myindex',
    meta: { title: '首页' },
    component: () => import('../components/MyIndex.vue'),

    children: [
      {
        path: '/myindex/index',
        name: 'index',
        meta: { title: '首页' },
        component: () => import('../views/index/index.vue')
      },
      {
        path: '/myindex/baobaotuan',
        name: 'baobaotuan',
        meta: { title: '爆爆团' },
        component: () => import('../views/index/baobaotuan.vue')
      },
      {
        path: '/myindex/dingdan',
        name: 'dingdan',
        meta: { title: '订单' },
        component: () => import('../views/index/dingdan.vue')
      },
      {
        path: '/myindex/wode',
        name: 'wode',
        meta: { title: '我的' },
        component: () => import('../views/index/wode.vue')
      },
      {
        path: '/myindex',
        redirect: '/myindex/index',
      },
    ]
  },
  {
    path: '/',
    redirect: '/mylogin',
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, form, next) => {
  next();
})

router.afterEach((to, form) => {
  document.title = to.meta.title;
})
export default router
