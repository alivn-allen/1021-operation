

import { Toast } from 'vant';

Toast('提示内容'); 文字提示

//todo 加载提示

Toast.loading({ message: '加载中...', forbidClick: true, });

//todo 成功 / 失败提示

Toast.success('成功文案');

Toast.fail('失败文案');


//todo 自定义图标

Toast({ message: '自定义图标', icon: 'like-o', });

Toast({ message: '自定义图片', icon: 'https://img01.yzcdn.cn/vant/logo.png', });
https://ts1.cn.mm.bing.net/th/id/R-C.75990ca9a3853bd3532e44b689d24675?rik=bVsNpyr9rFvhcg&riu=http%3a%2f%2fwx3.sinaimg.cn%2flarge%2f006APoFYly1go6s4emixlg301t01tweh.gif&ehk=rlYj%2b50wVQKXvUwlA6LF4qhxyM0dGd%2b2X9YE1T6gAFg%3d&risl=&pid=ImgRaw&r=0

//todo 自定义加载图标

Toast.loading({ message: '加载中...', forbidClick: true, loadingType: 'spinner', });


//todo 自定义位置

Toast({ message: '顶部展示', position: 'top', });

Toast({ message: '底部展示', position: 'bottom', });