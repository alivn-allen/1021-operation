import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { Form, Field, Button, Tabbar, TabbarItem, NavBar, Sidebar, SidebarItem, Icon } from 'vant';



Vue.use(Icon);
Vue.use(Sidebar);
Vue.use(SidebarItem);
Vue.use(NavBar);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Sidebar);
Vue.use(SidebarItem);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(NavBar);
Vue.use(Button);
Vue.use(Form);
Vue.use(Field);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
