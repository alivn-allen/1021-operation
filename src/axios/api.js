import service from "./request";

// 每一个请求 封装成一个函数

//  注册请求
export function user_register(params = {}) {
    return service.post('/user/register', params)
}

//  登录请求
export function user_login(params = {}) {
    return service.post('/user/login', params)
}

//  店铺详情请求
export function shopinfo_id(params = {}) {
    return service.get('/shop/shopinfo', { params })
}


//  店铺列表请求
export function shop_list(params = {}) {
    return service.get('/shop/list', { params })
}

//  爆爆团-列表请求
export function baobao_list(params = {}) {
    return service.get('/tuan/list', { params })
}





//* 模拟 post 请求实例
// export function user_login(params = {}) {
//     return service.get('/user/list', params)
// }