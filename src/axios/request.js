// 二次封装后的 axios
import axios from 'axios'
import { Toast } from 'vant';

// 实例化 axios 实例对象
const service = axios.create({
    timeout: 8 * 1000,                      //* 设置请求超时时间为 8s
    baseURL: '/api',                        //* 请求地址的基准路径
})

// 请求拦截器
service.interceptors.request.use(
    config => {                             //* 请求发出之前会立即自动执行的回调函数
        Toast.loading({ message: '加载中...', duration: 0 });
        // 统一设置请求头
        config.headers['Authorization'] = 'Bearer ' + localStorage.getItem('token');
        // config.headers['x-token'] = '';
        return config;
    },

    error => {                              //* 请求发出前，发生错误的回调函数
        return Promise.reject(error);
    }
)

// 响应拦截器
service.interceptors.response.use(

    Toast.clear(),
    response => {                           //* 请求服务器返回前会立即自动执行的回调函数
        return response;
    },

    error => {
        Toast.clear();                  //* 请求从服务器返回前，发生错误的回调函数
        if (error.response.status == 401) {
            Toast.fail('请求超时！')
            console.log('身份认证过期,请重新登陆！');
        }
        else if (error.response.status == 404) {
            Toast.fail('请求超时！')
            console.log('访问路径有误！');
        }
        else if (error.response.status == 500) {
            Toast.fail('请求超时！')
            console.log('服务器内部错误！请联系管理员处理！');
        }
    }
)

export default service;